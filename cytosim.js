// FJN, on June 2021 1--7 in Strasbourg

let points = 0, paths = 0;
let files0 = new Array(), files1 = new Array();
let sliderF, frame = 0;

function init()
{
    let B = document.getElementById('get-files');
    B.oninput = function(e){ attributeFiles(e.target.files); };
    let C = document.getElementById('clear');
    C.onclick = function(){ files0 = new Array(); files1 = new Array(); };
    sliderF = document.getElementById('sliderF');
    sliderF.oninput = function(e){ setFrame(e.target.value); };
}

function attributeFiles(list)
{
    for ( const f of list )
    {
        let str = f.name;
        let d = str.indexOf('.');
        if ( d > 4 )
        {
            let sub = str.substring(d-4, d);
            i = parseInt(sub);
            if ( str[d+1] == 'f' )
                files0[i] = f;
            else
                files1[i] = f;
            //console.log(`File ${str} has index ${i} <${sub}>`);
        }
    }
    let sup = Math.min(files0.length, files0.length);
    frame = Math.min(frame, sup);
    sliderF.max = sup;
    readFiles(files0[frame], files1[frame]);
}

function setFrame(f)
{
    if ( f != frame )
    {
        frame = f;
        readFiles(files0[f], files1[f]);
    }
}

function readFiles(f0, f1)
{
    if ( f0 && f1 )
    {
        //console.log('Files : ' + f0.name + '  &  ' + f1.name);
        let reader0 = new FileReader();
        let reader1 = new FileReader();
        reader0.onload = function(e) { getData(e.target.result, f0.name); };
        reader1.onload = function(e) { getData(e.target.result, f1.name); };
        reader0.onerror = function(e) { console.log('Error : ' + e.type); };
        reader1.onerror = function(e) { console.log('Error : ' + e.type); };
        num_paths = 0;
        points = 0;
        paths = 0;
        reader0.readAsArrayBuffer(f0);
        reader1.readAsArrayBuffer(f1);
    }
    else console.log(`Missing files for frame ${frame}`);
}

function convertFixed16(data)
{
    data = new Int16Array(data);
    const SCALE = 1.0 / 2048;
    let len = data.byteLength / 2;
    let flt = new Float32Array(len);
    for ( let i = 0; i < len; ++i )
         flt[i] = data[i] * SCALE;
    return flt;
}

function getData(data, filename)
{
    //console.log(`received ${data.byteLength} bytes from ${filename}`)
    let ext = filename.split('.').pop();
    if ( ext == 'paths32' )
        paths = new Uint32Array(data);
    else if ( ext == 'float32' )
        points = new Float32Array(data);
    else if ( ext == 'fixed16' )
        points = convertFixed16(data);
    if ( points && paths )
        makePaths(points, paths);
}
