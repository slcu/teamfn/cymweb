// FJN, on June 2021 1--7 in Strasbourg

let DIM = 3;
let zoom = 1;
let matrix = [ 1, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0 ];
let num_paths = 0, projected = 0;
let scene, front;

function main()
{
    init();
    scene = document.querySelector('#scene');
    front = scene.getContext('2d', { alpha: false });
    //scene.width = window.innerWidth - 14;
    //scene.height = window.innerHeight - 14;
    let toggle = document.querySelector('#toggle-dim');
    toggle.oninput = function(e){ if ( e.target.checked ) setDIM(2); else setDIM(3); };
    let sliderZ = document.getElementById('sliderZ');
    sliderZ.oninput = function(e){ setMatrix(e.target.value); };
    requestAnimationFrame(draw);
    setMatrix(50);
}

function project2D(M, pts)
{
    const len = pts.length - 1;
    let res = new Array(len+1);
    for ( let i = 0; i < len; i += 2 )
    {
        const X = pts[i  ];
        const Y = pts[i+1];
        res[i  ] = Math.floor( M[0] * X + M[3] * Y + M[9] );
        res[i+1] = Math.floor( M[1] * X + M[4] * Y + M[10] );
    }
    return res;
    //console.log(`projected ${(len+1)/2} points`);
}

function project3D(M, pts)
{
    const len = pts.length - 2;
    let res = new Array(len+2);
    for ( let i = 0; i < len; i += 3 )
    {
        const X = pts[i  ];
        const Y = pts[i+1];
        const Z = pts[i+2];
        res[i  ] = Math.floor( M[0] * X + M[3] * Y + M[6] * Z + M[9] );
        res[i+1] = Math.floor( M[1] * X + M[4] * Y + M[7] * Z + M[10] );
        res[i+2] = Math.floor( M[2] * X + M[5] * Y + M[8] * Z + M[11] );
    }
    return res;
    //console.log(`projected ${(len+2)/3} points`);
}

function projectPoints(pts)
{
    if ( pts.length >= DIM )
    {
        if ( DIM == 3 ) 
            projected = project3D(matrix, pts);
        else
            projected = project2D(matrix, pts);
        requestAnimationFrame(draw);
    }
}

function makePaths(points_in, paths_in)
{
    if ( paths_in.length > 0 )
    {
        projectPoints(points_in);
        points = points_in;
        paths = paths_in;
        num_paths = paths_in.length / 4;
        console.log(`compiled ${num_paths} paths for frame ${frame}`)
        requestAnimationFrame(draw);
    }
}

function setDIM(d)
{
    if ( d != DIM )
    {
        DIM = d;
        projectPoints(points);
        console.log('DIM: ' + DIM);
    }
    requestAnimationFrame(draw);
}

function setMatrix(Z)
{
    zoom = Z;
    matrix = [ Z, 0, 0, 0, -Z, 0, 0, 0, 1, 0.5*scene.width, 0.5*scene.height, 10 ];
    projectPoints(points);
    requestAnimationFrame(draw);
}


function drawBead(c, inx)
{
    //console.log(`drawing bead @ ${inx}`)
    c.beginPath();
    let R = Math.min(1, zoom*points[inx+DIM]);
    c.arc(projected[inx], projected[inx+1], R, 0, 2*Math.PI, true);
    c.stroke();
}

function drawPath(c, start, end)
{
    //console.log(`drawing path @ ${start} + ${end-start}`)
    c.beginPath();
    c.moveTo(projected[start], projected[start+1]);
    for ( let i = start+DIM; i <= end; i += DIM )
        c.lineTo(projected[i], projected[i+1]);
    c.stroke();
}

function drawPaths(c)
{
    c.lineWidth = 0.5;
    c.strokeStyle = 'white';
    
    //console.log(`drawing ${num_paths} paths`)
    for ( let i = 0; i < num_paths; ++i )
    {
        let s = paths[4*i+2], e = paths[4*i+3];
        if ( e <= s+1 )
            drawBead(c, s);
        else
            drawPath(c, s, e);
    }
}

function clearScene(c)
{
    //c.clearRect(0, 0, scene.width, scene.height);
    c.fillStyle = 'black';
    c.beginPath();
    c.rect(0, 0, scene.width, scene.height);
    c.fill();
}

function draw()
{
    if ( num_paths > 0 )
    {
        clearScene(front);
        drawPaths(front)
    }
}
